/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.Cliente;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
@WebServlet(name = "regist", urlPatterns = {"/regist"})
public class regist extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
        Banco banco =new Banco();
        if(request.getSession().getAttribute("banco")!=null){
            banco=(Banco) request.getAttribute("banco");
        }
        
        int cedula = Integer.parseInt(request.getParameter("cedula"));
        String nombre = request.getParameter("nombre");
        String dir = request.getParameter("direccion");
        String email = request.getParameter("email");
        String [] fecha = request.getParameter("fecha").split("-");
        int telefono = Integer.parseInt(request.getParameter("telefono"));
        Date fechas=new Date(Integer.parseInt(fecha[0]),Integer.parseInt(fecha[1]),Integer.parseInt(fecha[2]));

        Cliente c = new Cliente();
        c.setCedula(cedula);
        c.setNombre(nombre);
        c.setDircorrespondencia(dir);
        c.setEmail(email);
        c.setFechanacimiento(fechas);
        c.setTelefono(telefono);
        
        try {
            if(banco.insertarCliente(c)){
                request.getSession().setAttribute("banco", banco);
                request.getRequestDispatcher("./clientejsp/registroexistoso_1.jsp").forward(request, response);
            }else{
                request.getSession().setAttribute("error", "Dato ya registrado en el sistema");
                request.getRequestDispatcher("./jsp/error/error.jsp").forward(request, response);
            }
        } catch (ParseException ex) {
            Logger.getLogger(regist.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
