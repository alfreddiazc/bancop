/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

//import DAO.CajeroJpaController;
import DAO.ClienteJpaController;
import DAO.Conexion;
import DAO.CuentaJpaController;
//import DAO.MovimientoJpaController;
//import DAO.TipoJpaController;
import DAO.UsuarioJpaController;
//import DAO.TipoMovimientoJpaController;
import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
//import DTO.Cajero;
import DTO.Cliente;
import DTO.Cuenta;
//import DTO.Movimiento;
//import DTO.Tipo;
import DTO.Usuario;
//import Util.Test;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.pdmodel.PDPage;
//import org.apache.pdfbox.pdmodel.PDPageContentStream;
//import org.apache.pdfbox.pdmodel.common.PDRectangle;
//import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 *
 * @author Usuario
 */
public class Banco {
    
      private Usuario usu;
      private Conexion con;
      private boolean  inicio=false;

    public boolean isInicio() {
        return inicio;
    }
      
      
      
      
      
    
    public Banco() {
        con = Conexion.getConexion();
        usu=new Usuario();
    }
    public boolean iniciarSesion(String usuario,String contraseña){
        UsuarioJpaController cajp = new UsuarioJpaController(con.getBd());
        List<Usuario> usuarios = cajp.findUsuarioEntities();
        
        for (Usuario c : usuarios) {
            if (c.getUsuario().equals(usuario) && c.getPassword().equals(contraseña)) {
                
                usu = cajp.findUsuario(c.getId());
                inicio=true;
                return true;
            }
        }
        return false;
    }
    public boolean insertarCliente(Cliente c) throws ParseException{
        
        try {
            ClienteJpaController cj = new ClienteJpaController(con.getBd());
            cj.create(c);
            return true;
        } catch (Exception ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
//    
//    public boolean actualizarCliente(Cliente cliente){
//        ClienteJpaController cj = new ClienteJpaController(con.getBd());
//        try {
//            cj.edit(cliente);
//            return true;
//        } catch (NonexistentEntityException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (Exception ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//    
//    public boolean eliminarCliente(int cliente){
//        ClienteJpaController cj = new ClienteJpaController(con.getBd());
//        try {
//            cj.destroy(cliente);
//            return true;
//        } catch (IllegalOrphanException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NonexistentEntityException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//    
//    public boolean insertarCuenta(int nroCuenta, String fecha, int cedula, int tipo) throws ParseException{
//        ClienteJpaController cjp  = new ClienteJpaController(con.getBd());
//        TipoJpaController tjp = new TipoJpaController(con.getBd());
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//	Date fechan = dateFormat.parse(fecha);
//        Cliente cedulaAux = cjp.findCliente(cedula);
//        Tipo tipoAux = tjp.findTipo(tipo);
//        
//        Cuenta c = new Cuenta(nroCuenta, 0, fechan);
//        c.setCedula(cedulaAux);
//        c.setTipo(tipoAux);
//        
//        try {
//            CuentaJpaController cujp = new CuentaJpaController(con.getBd());
//            cujp.create(c);
//            return true;
//        } catch (Exception ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//        }        
//        return false;
//    }
//    
//    public boolean editarCuenta(Cuenta cuenta){
//        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
//        try {
//            cujp.edit(cuenta);
//            return true;
//        } catch (NonexistentEntityException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (Exception ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//    
//    public boolean eliminarCuenta(int nroCuenta){
//        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
//        try {
//            cujp.destroy(nroCuenta);
//            return true;
//        } catch (IllegalOrphanException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NonexistentEntityException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//    
//    public boolean registrarTransferencia(String fecha, int valor, int cuentaOrigen, int cuentaDestino, int tipo){
//        MovimientoJpaController mjp = new MovimientoJpaController(con.getBd());
//        try {
//            registrarMovimiento(fecha, valor, cuentaOrigen, tipo-1);
//            registrarMovimiento(fecha, valor, cuentaDestino, tipo-2);
//            return true;
//        } catch (ParseException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//    
//    public boolean registrarMovimiento(String fecha, int valor, int cuenta, int tipo) throws ParseException{
//        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
//        MovimientoJpaController mjp = new MovimientoJpaController(con.getBd());
//        TipoMovimientoJpaController tmjp = new TipoMovimientoJpaController(con.getBd());
//        Cuenta cu = cujp.findCuenta(cuenta);
//        int id = mjp.getMovimientoCount()+1;
//        if (tipo==2 && cu.getSaldo() >= valor) {
//            cu.setSaldo(cu.getSaldo() - valor);
//        }else if (tipo==1){
//            cu.setSaldo(cu.getSaldo() + valor);
//        }else{
//            return false;
//        }
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//	Date fechan = dateFormat.parse(fecha);
//        Movimiento mov = new Movimiento(id, fechan, valor);
//        
//        try {
//            cujp.edit(cu);
//            mov.setNroCuenta(cu);
//            mov.setIdTipoMovimiento(tmjp.findTipoMovimiento(tipo));
//            mjp.create(mov);
//        } catch (NonexistentEntityException ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (Exception ex) {
//            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    return true;
//    }
//    
//    public String extractoCliente(int cedula, String fechaInicio, String fechaFin) throws ParseException, IOException{
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//	Date fechaI = dateFormat.parse(fechaInicio);
//        Date fechaF = dateFormat.parse(fechaFin);
//        ClienteJpaController cjp  = new ClienteJpaController(con.getBd());
//        Cliente cedulaAux = cjp.findCliente(cedula);
//        String aux = "cedula: " + cedulaAux.getCedula() + ",nombre: " + cedulaAux.getNombre() + 
//                     ",fecha inicio: " + fechaI.toString() + ",fecha fin: " + fechaF.toString() + ",cuentas:[";
//        List<Cuenta> cuentas = cedulaAux.getCuentaList();
//        int iaux = 0;
//        for (Cuenta c : cuentas) {
//            String aux2 = "{numero:" + c.getNroCuenta() + ", tipo:" + c.getTipo().getNombre() + ", movimientos:[";
//            List<Movimiento> mov = c.getMovimientoList();
//            for (Movimiento m : mov) {
//                Date fm = m.getFecha();
//                if (fm.after(fechaI) && fm.before(fechaF)) {
//                        iaux++;
//                        aux2 += "{id:" + m.getId() + ", tipo:" + m.getIdTipoMovimiento().getDescripcion()+
//                            ", monto:" + m.getValor() + "},";
//                }
//            }
//            aux2 = aux2.substring(0,aux2.length()-1);
//            
//            if (iaux > 0) {
//                aux += aux2 + "]},"; 
//            }else{
//                aux += "\"No hay Movimientos\",";
//            }
//        }
//        aux = aux.substring(0,aux.length()-1);
//        aux += "]}";
//        makePdf(aux);
//        return aux;
//    }
//    
    public String getClientes(){
        ClienteJpaController cjp = new ClienteJpaController(con.getBd());
        List<Cliente> clientes = cjp.findClienteEntities();
        String aux = " ";
        for (Cliente c : clientes) {
            if (c != null) {
                aux += c.getCedula() + " - " + c.getNombre() + ";";
            }
        }
        return aux.substring(0, aux.length()-1);
    }
//    
//    public String getCliente(int cedula){
//        ClienteJpaController cjp = new ClienteJpaController(con.getBd());
//        Cliente c = cjp.findCliente(cedula);
//        return "cedula:" + c.getCedula() + ",nombre:" + c.getNombre() + ",direCorrespondencia:" + c.getDircorrespondencia()+
//                ",email:" + c.getEmail() + ",telefono:" +  c.getTelefono() + ",fechaNacimiento:" + c.getFechanacimiento();
//    }
//    
//    public String getCuenta(){
//        CuentaJpaController cujp = new CuentaJpaController(con.getBd());
//        List<Cuenta> cuentas = cujp.findCuentaEntities();
//        String aux = " ";
//        for (Cuenta c : cuentas) {
//            if (c != null) {
//                aux += c.getNroCuenta() + " - " + c.getCedula().getNombre() + ";";
//            }
//        }
//        return aux.substring(0, aux.length()-1);
//    }
//    
//    public boolean iniciarSesion(String usuario,String contraseña){
//        CajeroJpaController cajp = new CajeroJpaController(con.getBd());
//        List<Cajero> Cajeros = cajp.findCajeroEntities();
//        for (Cajero c : Cajeros) {
//            if (c.getUsuario().equals(usuario) && c.getContraseña().equals(contraseña)) {
//                cajero = cajp.findCajero(c.getId());
//                return true;
//            }
//        }
//        return false;
//    }
//    
//    private void makePdf(String cadena) throws IOException{
//         try (PDDocument document = new PDDocument()) {
//            PDPage page = new PDPage(PDRectangle.A6);
//            document.addPage(page);
//            PDPageContentStream contentStream = new PDPageContentStream(document, page);
//            String[] campos = cadena.split(",");
//            
//            // Text
//            contentStream.beginText();
//            contentStream.setFont(PDType1Font.TIMES_BOLD, 18);
//            contentStream.newLineAtOffset( 20, page.getMediaBox().getHeight() - 52);
//            contentStream.showText("Extracto bancario del cliente ocn " + campos[1] + campos[0] + " desde la " + campos[2] + " hasta la " + campos[3]);
//            contentStream.endText();
//            
//            contentStream.beginText();
//            contentStream.setFont(PDType1Font.TIMES_BOLD, 18);
//            contentStream.newLineAtOffset(20, page.getMediaBox().getHeight() - 80);
//             for (int i = 4; i < campos.length; i++) {
//                 contentStream.showText(campos[i]);
//             }
//            contentStream.endText();
//
//            contentStream.close();
//
//            document.save("C:\\extracto"+ campos[2] + "-" + campos[3] + LocalDate.now().toString() + ".pdf");
//        }
//    }
}
