
<%-- 
    Document   : registroexistoso
    Created on : 17/10/2019, 09:19:07 AM
    Author     : docente
--%>

<%@page import="DTO.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
        
        <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        
        <h1 class="register-title">Registro exitoso</h1>
        <br>
        <hr>
       
        
        <select class="">
            
            <option selected>selecione un cliente</option>
        <% 
            int cont=0;
            String [] str=banco.getClientes().split(";");
            for (String s:str)
        {
        %>
        <option value="<%=cont=cont+1%>"><%=s.toString()%></option>
            
        <%
            }
        %>
        </select>
        <hr>
        <a href="./jsp/Cliente/registrar.jsp">Ingresar otro cliente</a>
    </body>
</html>
